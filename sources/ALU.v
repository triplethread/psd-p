module Alu (a, b, c_in, c_out, out);
	// to add to numbers
	// Substract : use c_in to convert number to C2 (a) + (~b) + 1 = a - b
	input [7:0] a;
	input [7:0] b;
	input c_in;
	wire [8:0] rez;
	output [7:0] out;
	output c_out;
	
	assign rez = a + b + c_in;
	assign c_out = rez[8];
	assign out = rez [7:0];
	
endmodule
