
module AUTOMAT(clk, res, CS, WR, RD, FS, RDYP_ctrl, data_in, data_out, dbg);
	  parameter DSIZE = 7;	  
          input clk, res, CS;
	  input WR, RD;
	  input FS;
 	  input  [DSIZE : 0] data_in;
	  output [DSIZE : 0] data_out;
          output [DSIZE : 0] dbg;
          output RDYP_ctrl;

	  // interface output signals for Control Module
	  wire LDx, LDy;
	  wire ST;
	  wire OUTl, OUTh;
   	 wire[DSIZE : 0] dataOut_EEA;
	  
 	  // control ouput signals for EEA module
	  wire PLx, PLy, PLmin, PLmax, PLhigh, PLlow, PLaux, PLcounter;
	  wire SELcmp, SELlow, SELout;
	  wire DECcounter, DECaux, DECmax, DECmin;
	  wire INClow,INChigh;
          wire SHR;
		  
		wire resHigh;

	  // decision signals for control module
          wire minIs0, maxIs0, auxIs0, counterIs0;
	  wire overflow;
	  
	  //debug signals
          wire [4:0] state_next;
	  intf intf_automat (.clk(clk), .res(res), .CS(CS), .WR(WR), .RD(RD),
              .LDx(LDx), .LDy(LDy), .ST(ST), .OUTl(OUTl), .OUTh(OUTh), .rdyp(RDYP_ctrl));
	
	  EEA eea_automat ( 
            .clk(clk), .res(res),.resHigh(resHigh), // Input   
            .PLx(PLx), .PLy(PLy), .PLmin(PLmin), .PLmax(PLmax), .PLhigh(PLhigh), .PLlow(PLlow), .PLaux(PLaux), .PLcounter(PLcounter), // Paralel Load Signals
            .SELcmp(SELcmp), .SELlow(SELlow), .SELout(SELout),// MUX Selection
            .DECcounter(DECcounter), .DECaux(DECaux), .DECmax(DECmax), .DECmin(DECmin), // Decrement control
            .INClow(INClow), .INChigh(INChigh),// Increment control
            .SHR(SHR), // Shift right control
            .data_in(data_in), // Buffer In
            .data_out(dataOut_EEA), // Buffer Out
            //RDYP,
            .minIs0(minIs0), .maxIs0(maxIs0), .auxIs0(auxIs0), .counterIs0(counterIs0),
	    .overflow(overflow)
          );
          
          control control_automat ( .clk(clk), .res(res), .FS(unused), .LDx(LDx), .LDy(LDy), .ST(ST), 
		   .minIs0(minIs0), .maxIs0(maxIs0), .auxIs0(auxIs0), .counterIs0(counterIs0),
		   .OVF(overflow), .outL(OUTl), .outH(OUTh), 
		   .PLx(PLx), .PLy(PLy), .PLmin(PLmin), .PLmax(PLmax), .PLhigh(PLhigh), .PLlow(PLlow), .PLaux(PLaux), .PLcounter(PLcounter),
                   .SELcmp(SELcmp), .SELlow(SELlow), .SELout(SELout),
                   .DECcounter(DECcounter), .DECaux(DECaux), .DECmax(DECmax), .DECmin(DECmin),
                   .INClow(INClow), .INChigh(INChigh),
                   .SHR(SHR), .resHIGH(resHigh),
                   .state_next(state_next),// for debuging 
		   .RDYP(RDYP_ctrl)
	   );
	
	  //assign RDYP = RDYP_ctrl;
          // debuging
	  assign data_out = (RDYP_ctrl==0)?8'bZZZZ_ZZZZ: dataOut_EEA;
	  assign dbg [4:0] = state_next;
 	  assign dbg [5] = LDx;
	  assign dbg [6] = PLx;
	  assign dbg[7] = PLy;
endmodule 