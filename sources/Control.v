module control(
clk, res,
FS,
LDx,LDy,ST,
minIs0,maxIs0,counterIs0,auxIs0,
OVF,
outL,outH,
PLx,PLy,
PLmin, PLmax, PLhigh, PLlow, PLaux, PLcounter,
SELcmp, SELlow, SELout,
DECcounter, DECaux, DECmin, DECmax,
INClow, INChigh,
SHR,
RDYP,resHIGH,
state_next
 );
 // check for equivalent states TODO
	input clk, res;
	input FS; // don't know if we need this signal
	input LDx,LDy,ST;
	input minIs0,maxIs0,counterIs0,auxIs0;
	input OVF;
	input outL,outH;
	output reg PLx,PLy;
	output reg PLmin, PLmax, PLhigh, PLlow, PLaux, PLcounter;
	output reg SELcmp, SELlow, SELout;
	output reg DECcounter, DECaux, DECmin, DECmax;
	output reg INClow, INChigh;
	output reg SHR;
	output reg [4:0] state_next;
	output reg RDYP;
	output reg resHIGH;

	reg [4:0] state_crt;
	
	
	// model current state update 
	always@(negedge clk) begin
		if (res == 1'b0) begin
			state_crt = 5'b00000;
			end
		else
			state_crt = state_next;
	end

	// model next state computation
	always@(state_crt or LDx or LDy or FS or ST or minIs0 or maxIs0 or counterIs0 or auxIs0 or OVF or outL or outH)
	//always@(negedge clk)
 	begin
		casex({state_crt, LDx, LDy, ST, minIs0, maxIs0, counterIs0, auxIs0, OVF, outL, outH})
			// 0 -> 0/1
			15'b00000_1_x_x_x_x_x_x_x_x_x: begin state_next = 5'b00000; end
			15'b00000_0_x_x_x_x_x_x_x_x_x: begin state_next = 5'b00001; end
			// 1 -> 2 (read x)
			15'b00001_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b00010; end
			// 2 -> 2/3
			15'b00010_x_1_x_x_x_x_x_x_x_x: begin state_next = 5'b00010; end
			15'b00010_x_0_x_x_x_x_x_x_x_x: begin state_next = 5'b00011; end
			// 3 -> 4 (read y)
			15'b00011_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b00100; end
			
			// 4->4/5
			15'b00100_x_x_0_x_x_x_x_x_x_x: begin state_next = 5'b00100; end
			15'b00100_x_x_1_x_x_x_x_x_x_x: begin state_next = 5'b00101; end
			
			// 5 -> 11
			15'b00101_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b01011; end
			
			
			// 11 -> 6/7
			15'b01011_x_x_x_1_x_x_x_x_x_x: begin state_next = 5'b00110; end
			15'b01011_x_x_x_0_x_x_x_x_x_x: begin state_next = 5'b00111; end
			
			// 6-> 8/9
			15'b00110_x_x_x_x_1_x_x_x_x_x: begin state_next = 5'b01000; end
			15'b00110_x_x_x_x_0_x_x_x_x_x: begin state_next = 5'b01001; end
			
			// 7 -> 10/12
			15'b00111_x_x_x_x_1_x_x_x_x_x: begin state_next = 5'b01010; end			
			15'b00111_x_x_x_x_0_x_x_x_x_x: begin state_next = 5'b01100; end
			
			// 8 -> 11
			15'b01000_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b01011; end
			
			// 9 -> 12
			15'b01001_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b01100; end
			
	
			// 10 - > 12
			15'b01010_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b01100; end
			
			
			// 12 -> 13/14                                         
			15'b01100_x_x_x_1_1_x_x_x_x_x: begin state_next = 5'b01101; end
			15'b01100_x_x_x_x_0_x_x_x_x_x: begin state_next = 5'b01110; end
			15'b01100_x_x_x_0_x_x_x_x_x_x: begin state_next = 5'b01110; end
                                                            
			15'b01110_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b01111; end
			// 15 -> 16                                    
			15'b01111_x_x_x_x_x_x_x_0_x_x: begin state_next = 5'b10000; end
			// 15 -> 23                                     
			15'b01111_x_x_x_x_x_x_x_1_x_x: begin state_next = 5'b10111; end
			// 23 -> 16                                    
			15'b10111_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b10000; end
			// 16 -> 24                                       
			15'b10000_x_x_x_0_x_x_x_x_x_x: begin state_next = 5'b11000; end
			// 16-> 15                                 
			15'b10000_x_x_x_1_x_x_x_x_x_x: begin state_next = 5'b01111; end
			// 13 -> 17                                   
			15'b01101_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b10001; end
			
			// 17 -> 26
			15'b10001_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b11010; end
			
			
			// 26 -> 27      
			15'b11010_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b11011; end		
			
			// 18 -> 27
			15'b10010_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b11011; end
			
			// 27->19        
			15'b11011_x_x_x_0_x_x_x_x_x_x: begin state_next = 5'b10011; end
			15'b11011_x_x_x_x_x_x_0_x_x_x: begin state_next = 5'b10011; end  
			// 27 -> 18       
			15'b11011_x_x_x_1_x_x_1_x_x_x: begin state_next = 5'b10010; end
			
			// 19 -> 20      
			15'b10011_x_x_x_0_x_x_1_x_x_x: begin state_next = 5'b10100; end
			15'b10011_x_x_x_0_x_x_0_x_x_x: begin state_next = 5'b10100; end
			// 19 -> 21     
			15'b10011_x_x_x_1_x_x_0_x_x_x: begin state_next = 5'b10101; end
			// 20 -> 21        
			15'b10100_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b10101; end
			// 21 -> 22          
			15'b10101_x_x_x_x_x_x_x_x_x_x: begin state_next = 5'b10110; end  
			// 22 -> 17           
			15'b10110_x_x_x_x_x_1_x_x_x_x: begin state_next = 5'b10001; end
			// 22 -> 24       
			15'b10110_x_x_x_x_x_0_x_x_x_x: begin state_next = 5'b11000; end
			// 24 -> 25          
			15'b11000_x_x_x_x_x_x_x_x_0_x: begin state_next = 5'b11001; end
			// 24 -> 24
			15'b11000_x_x_x_x_x_x_x_x_1_x: begin state_next = 5'b11000; end
			// 25 -> 1 
			15'b11001_x_x_x_x_x_x_x_x_x_0: begin state_next = 5'b00000; end
			// 25 -> 25
			15'b11001_x_x_x_x_x_x_x_x_x_1: begin state_next = 5'b11001; end
			
			
			//Should never get here!
			default: begin state_next = 5'bxxxxx; end 		
		endcase
	end

	// model outputs
	//always@(state_crt) begin
	always@ (negedge clk) begin
		case({state_crt})
			5'b00000 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			5'b00001 : begin 
						 PLx			=	1'b0;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			5'b00010 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			5'b00011 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b0;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			5'b00100 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			//state 5
			5'b00101 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b0;
						 PLmax			=	1'b0;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 11
			5'b01011 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			// state 6
			5'b00110 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			// state 8
			5'b01000 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b0;
						 DECmax			=	1'b0;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			// state 7
			5'b00111 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			
			// state 10
			5'b01010 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b0;
						 PLmax			=	1'b0;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			// state 9
			5'b01001 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b0;
						 PLmax			=	1'b0;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 SELcmp			=	1'b0;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			// state 12
			5'b01100 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			// state 14
			5'b01110 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b0;
						 PLmax			=	1'b0;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b0;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			// state 15
			5'b01111 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b0;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b0;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b0;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 23
			5'b10111 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b0;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 16
			5'b10000 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			
			// state 13
			5'b01101 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b0;
						 PLmax			=	1'b1;
						 PLlow			=	1'b0;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b0;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b0;
			end
			// state 17
			5'b10001 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b0;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 26
			5'b11010 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b0;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b0;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 18
			5'b10010 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b0;
						 DECmin			=	1'b0;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 27
			5'b11011 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
		
			// state 19
			5'b10011 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 20
			5'b10100 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b0;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b0;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 21
			5'b10101 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b0;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 22
			5'b10110 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b0;
						 resHIGH		=	1'b1;
			end
			// state 24
			5'b11000 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b1;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b1;
						 resHIGH		=	1'b1;
			end
			// state 25
			5'b11001 : begin 
						 PLx			=	1'b1;
						 PLy			=	1'b1;
						 PLmin			=	1'b1;
						 PLmax			=	1'b1;
						 PLlow			=	1'b1;
						 PLaux			=	1'b1;
						 PLhigh			=	1'b1;
						 PLcounter		=	1'b1;
						 //SELcmp			=	1'b1;
						 SELlow			=	1'b1;
						 SELout			=	1'b0;
						 DECcounter		=	1'b1;
						 DECaux			=	1'b1;
						 DECmin			=	1'b1;
						 DECmax			=	1'b1;
						 INClow			=	1'b1;
						 INChigh		=	1'b1;
						 SHR			=	1'b1;
						 RDYP			=	1'b1;
						 resHIGH		=	1'b1;
			end
			
			//Should never get here!
			/*default: begin
						sela=1'b0;
                        selb=1'b0;
                        selc=1'b0; 
                        wsel=2'b00;
                        sel1=2'b00;
                        sel2=2'b00; 
                        dinw=1'b0;
			*/
		    //end 		
		endcase
		
	end		

endmodule
	
