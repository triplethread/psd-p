module EEA( 
            clk, res,resHigh, // Input   
            PLx, PLy, PLmin, PLmax, PLhigh, PLlow, PLaux, PLcounter, // Paralel Load Signals
            SELcmp, SELlow, SELout,// MUX Selection
            DECcounter, DECaux, DECmax, DECmin, // Decrement control
            INClow,INChigh,// Increment control
            SHR, // Shift right control
            data_in, // Buffer In
            data_out, // Buffer Out
            //RDYP,
            minIs0, maxIs0, auxIs0, counterIs0,
	    overflow
          );
  parameter DSIZE = 7;
  parameter OFF = 1'b1;
  input clk,res,resHigh;
  input PLx, PLy, PLmin, PLmax, PLhigh, PLlow, PLaux, PLcounter;
  input SELcmp, SELlow, SELout;
  input DECcounter, DECaux, DECmin, DECmax;
  input INClow, INChigh;
  input SHR;
  input [DSIZE : 0] data_in;
  
  wire [DSIZE : 0] data_x;
  wire [DSIZE : 0] data_y;

  wire [DSIZE : 0] data_mux_min;
  wire [DSIZE : 0] data_mux_max;

  wire [DSIZE : 0] data_reg_min;
  wire [DSIZE : 0] data_reg_max;
  
  wire [DSIZE : 0] data_reg_low;
  
  
  wire [DSIZE : 0] data_alu;
  wire [DSIZE : 0] data_mux_low;
  
  wire [DSIZE : 0] data_reg_high;
  wire [DSIZE : 0] data_reg_aux;

  wire [DSIZE : 0] data_mux_out;

  wire carry;
 
  wire unused;
  wire [DSIZE : 0] data_unused;
  
  wire resHigh_w;

  //output RDYP;
  output overflow;
  output minIs0, maxIs0, auxIs0, counterIs0;
  output [DSIZE:0] data_out;  
  // output [DSIZE:0] debug;


  Register  RegX (.clk(clk), .data_in(data_in), .carryIn(1'b0), .pl(PLx), .res(res), .SHR(OFF), .dec(OFF), .inc(OFF), .dIs0(unused), .data_out(data_x), .carryOut(unused));
  Register  RegY (.clk(clk), .data_in(data_in), .carryIn(1'b0), .pl(PLy), .res(res), .SHR(OFF), .dec(OFF), .inc(OFF), .dIs0(unused), .data_out(data_y), .carryOut(unused));
  
  // when SELcmp is 0 data_1 is selected
  mux_1_2 MuxMin (.data_1(data_y), .data_2(data_x), .sel(SELcmp), .out(data_mux_min));
  mux_1_2 MuxMax (.data_1(data_x), .data_2(data_y), .sel(SELcmp), .out(data_mux_max));

  Register RegMin (.clk(clk), .data_in(data_mux_min), .carryIn(1'b0), .pl(PLmin), .res(res), .SHR(OFF), .dec(DECmin), .inc(OFF), .dIs0(minIs0), .data_out(data_unused), .carryOut(unused));
  Register RegMax (.clk(clk), .data_in(data_mux_max), .carryIn(1'b0), .pl(PLmax), .res(res), .SHR(OFF), .dec(DECmax), .inc(OFF), .dIs0(maxIs0), .data_out(data_reg_max), .carryOut(unused));		

  Alu Adder (.a(data_reg_low), .b(data_reg_max), .c_in(1'b0), .c_out(overflow), .out(data_alu));
  mux_1_2 MuxLow (.data_1(data_alu), .data_2(data_reg_max), .sel(SELlow), .out(data_mux_low));

  Register RegLow (.clk(clk), .data_in(data_mux_low), .carryIn(1'b0), .pl(PLlow), .res(res), .SHR(SHR), .dec(OFF), .inc(INClow), .dIs0(unused), .data_out(data_reg_low), .carryOut(carry));		
  
  Register RegHigh (.clk(clk), .data_in(data_reg_aux), .carryIn(data_reg_low[DSIZE]), .pl(PLhigh), .res(resHigh_w), .SHR(SHR), .dec(OFF), .inc(INChigh), .dIs0(unused), .data_out(data_reg_high), .carryOut(unused));
  Register RegAux (.clk(clk), .data_in(data_reg_high), .carryIn(1'b0), .pl(PLaux), .res(res), .SHR(OFF), .dec(DECaux), .inc(OFF), .dIs0(auxIs0), .data_out(data_reg_aux), .carryOut(unused));
  
  Register RegCounter (.clk(clk), .data_in(8'b0000_1000), .carryIn(1'b0), .pl(PLcounter), .res(res), .SHR(OFF), .dec(DECcounter), .inc(OFF), .dIs0(counterIs0), .data_out(data_unused), .carryOut(unused));		
  
  mux_1_2 MuxOut (.data_1(data_reg_high), .data_2(data_reg_low), .sel(SELout), .out(data_mux_out));

  //official  
  assign data_out = data_mux_out;
  assign resHigh_w = (res & resHigh);
  
  
  //DEBUG
  //assign data_out = data_x;   [Works]
  //assign data_out = data_reg_low;
  //assign overflow = carry;
  //assign data_out = data_mux_out;
  //assign data_out = data_reg_max;         
endmodule
