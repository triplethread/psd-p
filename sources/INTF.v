
module intf(clk, res, CS, WR, RD,
            LDx, LDy, ST, OUTl, OUTh, rdyp);
  
  input clk, res, rdyp;
  input CS, WR, RD;
  output reg LDx, LDy, ST, OUTl, OUTh;
  reg[3:0] stare_cur, stare_urm;
  
  always@(negedge clk) begin
    if(res == 1'b0)
        stare_cur = 4'b0000;
    else
        stare_cur = stare_urm;
  end
  
  always@(stare_cur or WR or RD or CS or rdyp)begin           
    if(stare_cur == 4'b0000) begin
        casex({WR,CS})
            2'bx_0: stare_urm = 4'b0000;
            2'b0_1: stare_urm = 4'b0001; 
        endcase
    end
  
    if (stare_cur == 4'b0001) begin
        casex({WR,CS})
            2'bx_0: stare_urm = 4'b0000;
            2'b0_1: stare_urm = 4'b0001;
            2'b1_1: stare_urm = 4'b0010;                        
        endcase
    end
    
    if (stare_cur == 4'b0010) begin
        casex ({WR,CS})
            2'bx_0: stare_urm = 4'b0000; 
            2'b1_1: stare_urm = 4'b0010;
            2'b0_1: stare_urm = 4'b0011;
        endcase
    end
            
    if (stare_cur == 4'b0011) begin
        casex({WR,CS})
            2'bx_0: stare_urm = 4'b0000;
            2'b0_1: stare_urm = 4'b0011;
            2'b1_1: stare_urm = 4'b0100;
        endcase
    end
    
    if (stare_cur == 4'b0100) begin
        casex({RD,CS})
            2'bx_0: stare_urm = 4'b0000;
            //2'b1_1: stare_urm = 4'b0100;
            2'bx_1: stare_urm = 4'b1000;//4'b0101;
        endcase
    end
    
    if (stare_cur == 4'b1000) begin
        casex({rdyp,CS})
            2'bx_0: stare_urm = 4'b0000;
            2'b0_1: stare_urm = 4'b1000;
            2'b1_1: stare_urm = 4'b0101;
        endcase
    end
            
    if (stare_cur == 4'b0101) begin
        casex({RD,CS})
            2'bx_0: stare_urm = 4'b0000;
            2'b0_1: stare_urm = 4'b0101;
            2'b1_1: stare_urm = 4'b0110;
        endcase
    end
             
    if (stare_cur == 4'b0110) begin
        casex({CS})
            1'b0: stare_urm = 4'b0000;                
            1'b1: stare_urm = 4'b0111;
        endcase
    end
              
    if (stare_cur == 4'b0111) begin
        casex({CS})
            1'b0: stare_urm = 4'b0000;
            1'b1: stare_urm = 4'b0000;
        endcase
    end
  end

  
  always@(posedge clk) begin
    casex(stare_cur)
        4'b0000: {LDx,LDy,OUTl,OUTh,ST} = 5'b11110;
        4'b0001: {LDx,LDy,OUTl,OUTh,ST} = 5'b01110;
        4'b0010: {LDx,LDy,OUTl,OUTh,ST} = 5'b11110;
        4'b0011: {LDx,LDy,OUTl,OUTh,ST} = 5'b10110;
        4'b0100: {LDx,LDy,OUTl,OUTh,ST} = 5'b11111;
        4'b1000: {LDx,LDy,OUTl,OUTh,ST} = 5'b11110;
        4'b0101: {LDx,LDy,OUTl,OUTh,ST} = 5'b11010;
        4'b0110: {LDx,LDy,OUTl,OUTh,ST} = 5'b11110;
        4'b0111: {LDx,LDy,OUTl,OUTh,ST} = 5'b11100;
    endcase
  end
endmodule