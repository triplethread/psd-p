module mux_1_2(data_1, data_2, sel, out);
  parameter DIM = 7;
  input [DIM:0] data_1;
  input [DIM:0] data_2;
  
  input sel;
  
  output  [DIM:0] out;
  wire [DIM:0] out;
 
  
  assign out = (sel == 0)? data_1:data_2;
  
endmodule
