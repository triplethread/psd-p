module Register(clk, data_in, carryIn, pl, res, SHR, dec, inc, dIs0, data_out, carryOut);
	parameter D_SIZE = 7;
	input clk;
	input pl, res;
	input SHR;
	input dec;
	input inc;
	input carryIn;
	
	input [D_SIZE:0] data_in;

	
	output reg [D_SIZE:0] data_out;
	output dIs0;
	output reg carryOut;
	
	always @ ( negedge clk )
	begin	
		carryOut <= data_out[D_SIZE];
		casex ({res,pl,SHR,dec,inc})		
			5'b0xxxx:
			data_out <= 0;//RESET
			5'b10xxx:
			data_out <= data_in;// PARALLEL LOAD
			5'b110xx:
			begin		
			data_out <= (data_out << 1) | carryIn; //SHIFT RIGHT
			end
			5'b11101:
			if(data_out>0) 
			 data_out <= data_out - 1;// DECREMENT
			
			5'b11110:
			if(data_out<8'b1111_1111)
			 data_out <= data_out + 1;// INCREMENT
			
			default:
			data_out <= data_out;
			// HOLD
		endcase	
	end	
	assign dIs0 = (data_out == 0)? 1'b0:1'b1;

endmodule