module tb_ALU();
	reg [7:0] a;
	reg [7:0] b;
	reg c_in;
	wire c_out;
	wire [7:0] out;
	Alu test (.a(a), .b(b), .out(out), .c_in(c_in), .c_out(c_out));
	
	initial begin
	
	a = 8'b01110001;
	b = 8'b01111111;
	c_in = 1'b0;
	#20
	a= 8'b00000001;
	b = 8'b01111111;
	c_in = 1'b0;
	#20
	a= 8'b01000000;
	b = ~b;
	c_in = 1'b1;
	#20
	a= 8'b1111_1111;
	b=8'b1111_1111;
	
	#20
	$finish;
	end
endmodule