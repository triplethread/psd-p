module tb_AUTOMAT;
  parameter DSIZE = 7;

  reg clk, res, CS, WR, RD, FS;
  reg [DSIZE:0] data_in;

  wire RDYP; 
  wire [DSIZE:0] data_out;
  wire [DSIZE:0] dbg;

  AUTOMAT test(clk, res, CS, WR, RD, FS, RDYP, data_in, data_out, dbg);
  
  initial begin
        res = 1;
	CS = 0;
	WR = 1;
	RD = 1;
	FS = 0;
 	clk = 1;
    forever #10 clk=~clk;
	
     end
  initial begin
    #20 
    res = 0;
    #20
    res = 1;
    CS = 1;
    #20
    data_in = 39;
    FS = 0;
    #20
    // Load First Term
    WR = 0;
    #50
    WR = 1;
    #20
    data_in = 4;
    FS = 1;
    #20
    // Load Second Term
    WR = 0;
    #50
    WR = 1;
    #2000 
    $finish;
    
	
  end
endmodule
