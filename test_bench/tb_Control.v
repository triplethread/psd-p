module tb_Control;

reg clk;
reg res;
reg FS;
reg LDx, LDy, ST;
reg minIs0, maxIs0, counterIs0, auxIs0;
reg OVF;
reg outL, outH;
wire PLx,PLy;
wire PLmin, PLmax, PLhigh, PLlow, PLaux, PLcounter;
wire SELcmp, SELlow, SELout;
wire DECcounter, DECaux, DECmin, DECmax;
wire INClow, INChigh;
wire SHR;
wire [4:0] state_next;
wire RDYP;
wire resHIGH;

control test(
clk, res,
FS,
LDx,LDy,ST,
minIs0,maxIs0,counterIs0,auxIs0,
OVF,
outL,outH,
PLx,PLy,
PLmin, PLmax, PLhigh, PLlow, PLaux, PLcounter,
SELcmp, SELlow, SELout,
DECcounter, DECaux, DECmin, DECmax,
INClow, INChigh,
SHR,
RDYP,resHIGH,
state_next
 );

initial

begin
FS      = 1;
LDx     = 1;
res     = 1;
LDy     = 1;
ST      = 1;
minIs0  = 0;
maxIs0  = 0;
counterIs0 = 0;
auxIs0  = 0;
OVF     = 0;
outH    = 0;
outL	= 0;

forever #10 clk = ~clk;

end
 
initial begin

res = 0;
#20
//read x
FS      = 1;
LDx     = 1;
res     = 0;
LDy     = 1;
ST      = 1;
minIs0  = 0;
maxIs0  = 0;
counterIs0 = 0;
auxIs0  = 0;
OVF     = 0;
outH    = 0;
outL	= 0;

res = 0;
#20
//read x
FS      = 0;
LDx     = 0;
res     = 1;
LDy     = 1;
ST      = 1;
minIs0  = 0;
maxIs0  = 0;
counterIs0 = 0;
auxIs0  = 0;
OVF     = 0;
outH    = 0;
outL	= 0;

#20
//read y
FS      = 1;
LDx     = 1;
res     = 1;
LDy     = 0;
ST      = 1;
minIs0  = 1;
maxIs0  = 1;
counterIs0 = 1;
auxIs0  = 1;
OVF     = 1;
outH    = 1;
outL	= 1;

#20

FS      = 1;
LDx     = 0;
res     = 1;
LDy     = 0;
ST      = 0;
minIs0  = 0;
maxIs0  = 0;
counterIs0 = 0;
auxIs0  = 0;
OVF     = 0;
outH    = 0;
outL	= 0;

#20

FS      = 0;
LDx     = 0;
res     = 0;
LDy     = 1;
ST      = 0;
minIs0  = 1;
maxIs0  = 0;
counterIs0 = 1;
auxIs0  = 1;
OVF     = 1;
outH    = 0;
outL	= 0;

#20

FS      = 1;
LDx     = 0;
res     = 1;
LDy     = 1;
ST      = 1;
minIs0  = 1;
maxIs0  = 0;
counterIs0 = 1;
auxIs0  = 0;
OVF     = 1;
outH    = 0;
outL	= 0;

#20

FS      = 1;
LDx     = 1;
res     = 1;
LDy     = 0;
ST      = 0;
minIs0  = 1;
maxIs0  = 0;
counterIs0 = 1;
auxIs0  = 1;
OVF     = 1;
outH    = 0;
outL	= 0;

#20

FS      = 1;
LDx     = 1;
res     = 1;
LDy     = 1;
ST      = 1;
minIs0  = 0;
maxIs0  = 0;
counterIs0 = 1;
auxIs0  = 1;
OVF     = 1;
outH    = 0;
outL	= 0;

#20

FS      = 1;
LDx     = 1;
res     = 1;
LDy     = 1;
ST      = 1;
minIs0  = 0;
maxIs0  = 1;
counterIs0 = 1;
auxIs0  = 1;
OVF     = 1;
outH    = 0;
outL	= 0;


#80

FS      = 1;
LDx     = 1;
res     = 1;
LDy     = 1;
ST      = 1;
minIs0  = 1;
maxIs0  = 1;
counterIs0 = 0;
auxIs0  = 1;
OVF     = 0;
outH    = 1;
outL	= 0;
$finish;
end


endmodule