module tb_EEA;
  parameter DSIZE = 7;
  reg clk,res,resHigh;
  reg PLx, PLy, PLmin, PLmax, PLhigh, PLlow, PLaux, PLcounter;
  reg SELcmp, SELlow, SELout;
  reg DECcounter, DECaux, DECmin, DECmax;
  reg INClow, INChigh;
  reg SHR;
  reg [DSIZE : 0] data_in;
  
  //wire RDYP;
  wire overflow;
  wire minIs0, maxIs0, auxIs0, counterIs0;
  wire [DSIZE:0] data_out;
  
  EEA test ( 
            clk, res, resHigh, // Input   
            PLx, PLy, PLmin, PLmax, PLhigh, PLlow, PLaux, PLcounter, // Paralel Load Signals
            SELcmp, SELlow, SELout,// MUX Selection
            DECcounter, DECaux, DECmax, DECmin, // Decrement control
            INClow,INChigh,// Increment control
            SHR, // Shift left control
            data_in, // Buffer In
            data_out, // Buffer Out
            //RDYP,
            minIs0, maxIs0, auxIs0, counterIs0,
	    overflow
          );
  initial begin
  res = 1;
  resHigh = 1;
  PLx = 1;
  PLy = 1;
  PLmin = 1;
  PLmax = 1;
  PLlow = 1;
  PLhigh = 1;
  PLaux = 1;
  PLcounter = 1;
  SELcmp = 1;
  DECmin = 1;
  DECmax = 1;
  DECcounter = 1;
  DECaux = 1;
  SELlow = 1;
  SELout = 1;   
  clk = 1;
  INClow = 1;
  INChigh = 1;
  SHR = 1;
  forever #10 clk = ~clk;

  
  end
  initial begin
  #20
  res = 0;
  data_in = 8'b0000_0101;
  #20
  res = 1; // reset is off
  #20 
  // Load Data in Reg X
  PLx = 0; //[Works]
  #20
  PLx = 1;
  #20
  
  // Load Data in Reg Y
  data_in = 8'b0100_0011;
  #20
  PLy = 0; //[Works]
  #20
  PLy = 1;
  
  // Test Selcmp = 1 ( data_x = min, data_y = max )
  #20
  // load Reg Min and Reg Max when Selcmp = 1
  PLmin = 0; //[Works]
  PLmax = 0; //[Works]
  #20
  PLmin = 1;
  PLmax = 1;
  #20 
  
  // Decrement until Reg Min, Reg Max are 0, Test Signal minIs0 and maxIs0 [Works]
  // Check if Reg Max is loaded with the corect value ( Reg_Y for SELcmp = 1 ) [Works]
  #20
  DECmin = 0; //[Works]
  DECmax = 0; //[Works]

  #120
  DECmin = 1;
  DECmax = 1;
  #20
  // Load Reg Max
  PLmax = 0;
  #20
  PLmax = 0;
  #20
  // Reg Low is set for output //[Works]
  SELlow = 1; //[Works]
  SELout = 1; //[Works]
  // Load Reg Low with Reg Max value //[Works]
  #20
  PLlow = 0; //[Works]
  #20
  PLlow = 1;
  #20 
  // Load Reg Low with value from Adder //[Works]
  #20
  SELlow = 0;
  #20
  PLlow = 0;//[Works]
  #80
  PLlow = 1;
  // Add until OVF //[Works]
  #20
  PLlow = 0;
  #100
  PLlow = 1; 
  // Shift Righ Test :
  // Load Reg Low from Reg Max
  // Set Reg High for output //[Works]
  // Shift until Reg High has the value from Reg Low //[Works]
  #20
  SELlow = 1;
  SELout = 0;
  #20
  PLlow = 0;
  #20
  PLlow = 1;
  #20
  SHR = 0;
  #180
  SHR = 1;
  #20
  //TODO
  /// Load Reg High from Reg Aux //[Works]
  PLhigh = 0;
  // Load Reg Aux from Reg High	//[Works]
  #20
  PLhigh = 1;
  #20
  INChigh = 0; // Increment High Register //[Works]
  #60
  INChigh = 1; 
  #20
  PLaux = 0;
  #20
  PLaux = 1;
  #20
  INChigh = 0;
  #60
  INChigh = 1;
  #20
  PLhigh = 0;
  #20
  PLhigh = 1;
  #20
  PLcounter = 0;
  #20
  PLcounter = 1;
  #20
  DECcounter = 0; // Decrement Counter Register //[Works]
  #140
  DECcounter = 1;
  #20
  $finish;
  end
endmodule
