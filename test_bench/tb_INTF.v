module tb_intf();

  reg clk, res, CS, WR, RD, rdyp;
  wire LDx, LDy, OUTl, OUTh, ST;
  intf sist(clk, res, CS, WR, RD,
              LDx, LDy, ST, OUTl, OUTh, rdyp);
  
  initial begin
    forever #10 clk=~clk;
  end
  initial begin
    #0  res=0;CS=0;WR=0;RD=0;clk=0;rdyp=0;
    #25 res=1;CS=1;                        //0000
    #20 WR=1;                              //0001
    #20 WR=0;                              //0010
    #20 WR=1;                              //0011
    #20 RD=1;                              //0100
    #20 rdyp=1;                            //1000
    #20 RD=1;                              //0101
    #20 RD=0;                              //0110
    #20 RD=1;                              //0111
    #100 $finish;
  end
endmodule