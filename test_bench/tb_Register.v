module tb_Register();

	parameter DSIZE = 7;
	reg clk;
	reg pl;
	reg res;
	reg shL;
	reg dec;
	reg inc;
	reg [DSIZE:0] data_in;
	reg [DSIZE:0] d;
	reg carryIn;

	wire [DSIZE:0] out;
	wire dIsZero;	
	wire carryOut;
	Register test (.clk(clk), .data_in(data_in), .carryIn(carryIn), .pl(pl), .res(res), .SHR(shL), .dec(dec), .dIs0(dIsZero), .carryOut(carryOut), .data_out(out),.inc(inc)); 
	initial begin 
		pl = 1;
		res = 1;
		shL = 1;
		dec = 1;
		inc = 1;
		data_in = 0;
		d=0;
		carryIn = 0;
		clk = 1;
		forever #10 clk = ~clk;
	end 
	initial begin
	//RESET
		#5
		res = 0;
		data_in = 8'b0000_0010;	
		#20
		
		
	//Parallel Load
		#40
		res = 1;
		pl = 0;
		#10
		data_in = 8'b0001_1110;
		#20
	//Decrement
		pl = 1;
		dec = 0;
		#200
	//Shift Right
		dec = 1;
		#20
		shL = 0;
		#60
		res = 0;
		#20
		res = 1;
		#100
	//Hold
		$finish;
	end


endmodule
