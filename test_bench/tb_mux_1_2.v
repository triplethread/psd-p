module tb_mux_1_2();
  parameter DIM = 7;
  
  reg [DIM:0] data_1;
  reg [DIM:0] data_2;
  reg sel;
  
  wire [DIM:0] out;
  
  mux_1_2 test (.data_1(data_1), .data_2(data_2), .sel(sel), .out(out));
  initial begin
    data_1 = 8'b0000_1111;
    data_2 = 8'b0000_0001;
    
    sel = 1'b1;
    
    #20
    
    sel = 1'b0;
    
    #20
    
    sel = 1'bx;
    
    $finish;
  end


endmodule
